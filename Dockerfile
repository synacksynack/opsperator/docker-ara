FROM docker.io/python:3-slim-buster

# Ansible Runtime Analysis Image for OpenShift Origin

ARG DO_UPGRADE=
ENV ARA_VERSION=1.5.8 \
    PSY_VERSION=2.8.6 \
    DEBIAN_FRONTEND=noninteractive

LABEL io.k8s.description="ARA Collects and Archives Ansible Reports." \
      io.k8s.display-name="ARA $ARA_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="ara,ansible" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-ara" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$ARA_VERSION"

COPY config/* /

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && if test "$DEBUG"; then \
	echo "# Install Debugging Tools" \
	&& apt-get -y install psutils; \
    fi \
    && echo "# Install Ara Dependencies" \
    && mkdir -p /usr/share/man/man1 /usr/share/man/man7 \
    && apt-get install -y gcc python-dev libffi-dev libssl-dev curl \
	mariadb-client postgresql-client libpq-dev dumb-init \
    && echo "# Install Ara" \
    && pip install psycopg2==$PSY_VERSION pymysql \
	ara[server]==$ARA_VERSION \
    && mkdir -p /.ansible/tmp /.ara/server \
    && echo "# Fixing Permissions" \
    && chmod -R 0775 /tmp /.ansible /.ara \
    && chown -R 1001:root /tmp /.ansible /.ara \
    && echo "# Cleaning up" \
    && apt-get remove --purge -y gcc python-dev libssl-dev libffi-dev \
	libpq-dev \
    && apt-get autoremove --purge -y \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT ["dumb-init","--","/run-ara.sh"]
USER 1001
