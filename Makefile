SKIP_SQUASH?=1
FRONTNAME=opsperator
IMAGE=opsperator/ara
-include Makefile.cust

.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	@@docker rm -f testara || true
	@@docker run -e DB_TYPE=sqlite --name testara -d $(IMAGE)

.PHONY: run
run:
	@@docker run -d \
	    -e POSTGRESQL_USER=ara -e POSTGRESQL_PASSWORD=ara \
	    -e POSTGRESQL_DATABASE=ara -e POSTGRESQL_ADMIN_PASSWORD=aradmin \
	    -p 5432:5432 docker.io/centos/postgresql-10-centos7:latest
	@@sleep 10
	@@MAINDEV=`ip r | awk '/default/' | sed 's|.* dev \([^ ]*\).*|\1|'`; \
	MAINIP=`ip r | awk "/ dev $$MAINDEV .* src /" | sed 's|.* src \([^ ]*\).*$$|\1|'`; \
	docker run -e POSTGRES_DB=ara -e POSTGRES_HOST=$$MAINIP \
	    -e POSTGRES_PASSWORD=ara -e POSTGRES_USER=ara $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
deployuild: kubecheck
	@@for f in image secret service deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "ARA_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "ARA_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	oc process -f deploy/openshift/run-persistent.yaml -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	oc process -f deploy/openshift/secret.yaml -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	if ! oc describe secret ara-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml -p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	oc process -f deploy/openshift/run-ephemeral.yaml -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@if ! oc describe secret ara-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocprod
ocprod: ocdemopersistent

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
