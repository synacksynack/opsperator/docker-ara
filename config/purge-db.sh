#!/bin/sh

if test -z "$ARA_PRUNE_COMPLETED_AFTER"; then
    ARA_PRUNE_COMPLETED_AFTER="15 days"
fi
if test -z "$ARA_PRUNE_PENDING_AFTER"; then
    ARA_PRUNE_PENDING_AFTER="7 days"
fi

if test "$DB_TYPE" = mysql -o "$MYSQL_DB" -o "$MYSQL_HOST"; then
    MYSQL_DB=${MYSQL_DB:-ara}
    MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
    MYSQL_PASSWORD="${MYSQL_PASSWORD:-secret}"
    MYSQL_PORT=${MYSQL_PORT:-3306}
    MYSQL_USER=${MYSQL_USER:-ara}
    (
	cat <<EOF
SELECT playbook_id FROM plays
WHERE (status = 'running' AND ended IS NULL
       AND created < NOW() - INTERVAL $ARA_PRUNE_PENDING_AFTER)
      OR (status = 'completed'
	  AND ended < NOW() - INTERVAL $ARA_PRUNE_COMPLETED_AFTER)
EOF
    ) | mysql \
	    -u "$MYSQL_USER" \
	    --password="$MYSQL_PASSWORD" \
	    -h "$MYSQL_HOST" \
	    -P "$MYSQL_PORT" \
	    "$MYSQL_DB" 2>/dev/null \
	| sed 's| ||g' \
	| while read playbook_id
	do
	    test "$playbook_id" -ge 0 2>/dev/null || continue
	    cat <<EOF
DELETE FROM results WHERE playbook_id = $playbook_id;
DELETE FROM hosts WHERE playbook_id = $playbook_id;
DELETE FROM tasks WHERE playbook_id = $playbook_id;
DELETE FROM plays WHERE playbook_id = $playbook_id;
DELETE FROM files WHERE playbook_id = $playbook_id;
DELETE FROM playbooks_labels WHERE playbook_id = $playbook_id;
DELETE FROM playbooks WHERE id = $playbook_id;
EOF
	done \
	| mysql \
	    -u "$MYSQL_USER" \
	    --password="$MYSQL_PASSWORD" \
	    -h "$MYSQL_HOST" \
	    -P "$MYSQL_PORT" \
	    "$MYSQL_DB"
else
    POSTGRES_DB=${POSTGRES_DB:-ara}
    POSTGRES_HOST=${POSTGRES_HOST:-127.0.0.1}
    POSTGRES_PASSWORD="${POSTGRES_PASSWORD:-secret}"
    POSTGRES_PORT=${POSTGRES_PORT:-5432}
    POSTGRES_USER=${POSTGRES_USER:-ara}
    (
	cat <<EOF
SELECT playbook_id FROM plays
WHERE (status = 'running' AND ended IS NULL
       AND created < NOW() - INTERVAL '$ARA_PRUNE_PENDING_AFTER')
      OR (status = 'completed'
	  AND ended < NOW() - INTERVAL '$ARA_PRUNE_COMPLETED_AFTER')
EOF
    ) | PGPASSWORD="$POSTGRES_PASSWORD" psql \
	    -U "$POSTGRES_USER" \
	    -h "$POSTGRES_HOST" \
	    -p "$POSTGRES_PORT" \
	    -t "$POSTGRES_DB" 2>/dev/null \
	| sed 's| ||g' \
	| while read playbook_id
	do
	    test "$playbook_id" -ge 0 2>/dev/null || continue
	    cat <<EOF
DELETE FROM results WHERE playbook_id = $playbook_id;
DELETE FROM hosts WHERE playbook_id = $playbook_id;
DELETE FROM tasks WHERE playbook_id = $playbook_id;
DELETE FROM plays WHERE playbook_id = $playbook_id;
DELETE FROM files WHERE playbook_id = $playbook_id;
DELETE FROM playbooks_labels WHERE playbook_id = $playbook_id;
DELETE FROM playbooks WHERE id = $playbook_id;
EOF
	done | PGPASSWORD="$POSTGRES_PASSWORD" psql \
	    -U "$POSTGRES_USER" \
	    -h "$POSTGRES_HOST" \
	    -p "$POSTGRES_PORT" \
	    -t "$POSTGRES_DB"
fi
