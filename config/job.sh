#!/bin/sh

if test "$DEBUG"; then
    set -x
    JOB_INTERVAL=3600
fi
JOB_INTERVAL=${JOB_INTERVAL:-86400}
if test -z "$ARA_PRUNE_COMPLETED_AFTER"; then
    ARA_PRUNE_COMPLETED_AFTER="15 days"
fi
if test -z "$ARA_PRUNE_PENDING_AFTER"; then
    ARA_PRUNE_PENDING_AFTER="7 days"
fi

while :
do
    started=$(date +%s)
    . /purge-db.sh
    stopped=$(date +%s)
    elapsed=`expr $stopped - $started`
    echo "Done pruning records (${elapsed}s). Sleeping for ${JOB_INTERVAL}s"
    sleep $JOB_INTERVAL
done

exit 42
