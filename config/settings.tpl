default:
  ALLOWED_HOSTS:
  - ::1
  - ARA_FQDN
  - ARA_HNAME
  - 127.0.0.1
  - localhost
  BASE_DIR: /.ara/server
  CORS_ORIGIN_ALLOW_ALL: CORS_ALLOW_ALL
  CORS_ORIGIN_REGEX_WHITELIST: []
  CORS_ORIGIN_WHITELIST:
  - http://127.0.0.1:BINDPORT
  - http://127.0.0.1
  - http://localhost:BINDPORT
  - http://localhost
  - http://ARA_HNAME:BINDPORT
  - https://ARA_FQDN
  DATABASE_CONN_MAX_AGE: 0
  DATABASE_ENGINE: DBENGINE
  DATABASE_HOST: DBHOST
  DATABASE_NAME: DBNAME
  DATABASE_PASSWORD: DBPASSWORD
  DATABASE_PORT: DBPORT
  DATABASE_USER: DBUSER
  DEBUG: DODEBUG
  DISTRIBUTED_SQLITE: false
  DISTRIBUTED_SQLITE_PREFIX: ara-report
  DISTRIBUTED_SQLITE_ROOT: /var/www/logs
  LOGGING:
    disable_existing_loggers: false
    formatters:
      normal:
        format: '%(asctime)s %(levelname)s %(name)s: %(message)s'
    handlers:
      console:
        class: logging.StreamHandler
        formatter: normal
        level: INFO
        stream: ext://sys.stdout
    loggers:
      ara:
        handlers:
        - console
        level: INFO
        propagate: 0
    level: INFO
    version: 1
  LOG_LEVEL: INFO
  PAGE_SIZE: 100
  READ_LOGIN_REQUIRED: false
  SECRET_KEY: DJANGOSECRET
  TIME_ZONE: DJANGOTZ
  WRITE_LOGIN_REQUIRED: false
